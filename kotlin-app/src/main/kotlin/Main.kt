package org.example.gitlabcourse.featureflags

import io.getunleash.DefaultUnleash
import io.getunleash.util.UnleashConfig

fun main(args: Array<String>) {
    val unleashApiUrl = "https://gitlab.com/api/v4/feature_flags/unleash/53589219"
    val unleashInstanceId = "shQfh8WyveGyX5kW27Qp"
    println("Hello, World!")
    val environmentNames = listOf("test", "production")
    for (appEnvironmentName in environmentNames) {
        println("==== Environment ${appEnvironmentName} ====")
        val config = UnleashConfig.builder()
            .appName(appEnvironmentName)
            .instanceId(unleashInstanceId)
            .unleashAPI(unleashApiUrl)
            .build()

        val unleash = DefaultUnleash(config)

        val features = listOf("fizzbuzz", "foobar-test", "foobar-production")

        for (feature in features) {
            if (unleash.isEnabled(feature)) {
                println("[X] Feature ${feature} enabled in '${appEnvironmentName}' environment")
            } else {
                println("[ ] Feature ${feature} disabled in '${appEnvironmentName}' environment")
            }
        }

        var okCount = 0;
        val totalCount = 50;
        for (i in 1..totalCount) {
            if (unleash.isEnabled("pingpong-fiftyfifty")) {
                okCount++
            }
        }
        println("[%] Feature pingpong-fiftyfifty enabled in '${appEnvironmentName}' environment on ${okCount}/${totalCount}")
    }
}