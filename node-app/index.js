// node.js
const { Unleash } = require('unleash-client')

const unleashApiUrl = "https://gitlab.com/api/v4/feature_flags/unleash/53589219"
const unleashInstanceId = "shQfh8WyveGyX5kW27Qp"
const environmentNames = ["test", "production"]

for(let appEnvironment of environmentNames) {	
	const unleash = new Unleash({
	  url: unleashApiUrl,
	  appName: appEnvironment,
	  instanceId: unleashInstanceId
	});

	unleash.on('ready', () => {
		console.log.bind(console, 'ready')
		const features = ["fizzbuzz", "foobar-test", "foobar-production"]
		for(let f of features) {
			if (unleash.isEnabled(f)) {
		    	console.log(`[X] Feature ${f} enabled in '${appEnvironment}' environment`)
		  } else {
		    	console.log(`[ ] Feature ${f} disabled in '${appEnvironment}' environment`)
		  }
		}

		let okCount = 0;
		let totalCount = 50;
		for(let i = 0; i < totalCount; i++) {
			if(unleash.isEnabled("pingpong-fiftyfifty")) {
				okCount++;
			}			
		}
		console.log(`[%] Feature pingpong-fiftyfifty enabled in '${appEnvironment}' environment on ${okCount}/${totalCount}`)

		const perUserFeatures = ["new-feature-only-for-devs", "exciting-new-feature"]
		let users = ['pietrom', 'cristinar', 'martinellip', 'admin', 'pietro', 'goofy']
		for(let f of perUserFeatures) {
			for(let user of users) {
				if(unleash.isEnabled(f, { userId: user })) {
					console.log(`[X] Feature ${f} enabled for user ${user} in ENV ${appEnvironment}`)
				} else {
					console.log(`[ ] Feature ${f} disabled for user ${user} in ENV ${appEnvironment}`)
				}
			}
		}
	});
	unleash.on('error', console.error);
}
