import React from 'react'
import { useFlag } from '@unleash/proxy-client-react';

export default function Feature({children, feature}) {
    if(useFlag(feature)) {
        return <>{children}</>
    }
    return <div><em>Feature {feature} disabled</em></div>
}