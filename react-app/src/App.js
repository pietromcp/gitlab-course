import {FlagProvider} from '@unleash/proxy-client-react';
import './App.css';
import FizzBuzz from './features/FizzBuzz'
import Feature from './features/Feature'
import PingPong from './features/PingPong'
import FooBar from './features/FooBar'

const unleashTestConfig = {
    url: ' http://localhost:4000/proxy', // Your front-end API URL or the Unleash proxy's URL (https://<proxy-url>/proxy)
    clientKey: 'the-test-sercret', // A client-side API token OR one of your proxy's designated client keys (previously known as proxy secrets)
    refreshInterval: 60, // How often (in seconds) the client should poll the proxy for updates
    appName: 'react-app-test', // The name of your application. It's only used for identifying your application
};

const unleashProductionConfig = {
    url: 'http://localhost:4001/proxy', // Your front-end API URL or the Unleash proxy's URL (https://<proxy-url>/proxy)
    clientKey: 'the-production-sercret', // A client-side API token OR one of your proxy's designated client keys (previously known as proxy secrets)
    refreshInterval: 60, // How often (in seconds) the client should poll the proxy for updates
    appName: 'react-app-production', // The name of your application. It's only used for identifying your application
};

function App() {
    return (
        <div className="App">
            <header className="App-header">
                TEST
                <FlagProvider config={unleashTestConfig}>
                    <Feature feature="fizzbuzz"><FizzBuzz/></Feature>
                    <Feature feature="pingpong-fiftyfifty"><PingPong/></Feature>
                    <Feature feature="foobar-test"><FooBar/></Feature>
                    <Feature feature="foobar-production"><FooBar/></Feature>
                </FlagProvider>
                <hr width={100}/>
                PROD
                <FlagProvider config={unleashProductionConfig}>
                    <Feature feature="fizzbuzz"><FizzBuzz/></Feature>
                    <Feature feature="pingpong-fiftyfifty"><PingPong/></Feature>
                    <Feature feature="foobar-test"><FooBar/></Feature>
                    <Feature feature="foobar-production"><FooBar/></Feature>
                </FlagProvider>
            </header>
        </div>
    );
}

export default App;
